﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Tools-->Nuget-->manager nuget-->browse 搜“entity framework" 110M那个，安装
//Tools-->Nuget-->Package Manager console-->在代码行粘：enable-migrations -EnableAutomaticMigrations:$true
//DB自动创建在 C: \Users\chenh\，  Data Connection里add connection选这个位置里的db

namespace Day10FirstEF
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                SocietyDbContext ctx = new SocietyDbContext();
                Random random = new Random();

                //equivalent of insert
                Person p1 = new Person { Name = "Jerry", Age = random.Next(100) };

                ctx.People.Add(p1);  //insert operation is scheduled but not executed yet
                ctx.SaveChanges();  //synchronize objects in memory with database
                Console.WriteLine("record added");

                //equivalent of update--fetch then modify, save changes
                Person p2 = (from p in ctx.People where p.Id == 2 select p).FirstOrDefault<Person>();
                if (p2 != null)
                {  //found the record to update
                    p2.Name = "Alabama" + (random.Next(10000) + 10000); //entity framework is watching and notices the modification
                    ctx.SaveChanges();  //update the database to synchronize entities in memory with the database
                    Console.WriteLine("Update record");
                }
                else
                {
                    Console.WriteLine("record to update not found");
                }

                //delete- fetch then schedule for deletion, then save changes
                Person p3 = (from p in ctx.People where p.Id == 3 select p).FirstOrDefault<Person>();
                if (p3 != null)
                { //found the record to delete
                    ctx.People.Remove(p3);
                    ctx.SaveChanges();
                    Console.WriteLine("Record Deleted");
                }
                else
                {
                    Console.WriteLine("record to delete not found");
                }

                //fetch all records
                List<Person> peopleList = (from p in ctx.People select p).ToList<Person>();
               
                foreach (Person p in peopleList)    //not modify, not need index可以用foreach
                {
                    Console.WriteLine($"{p.Id}:{p.Name} is {p.Age} y/o");
                }
            }
            catch(SystemException ex)  //catch-all for EF, SQL and many other exceptions  这个课的都catch这个exception就可以，他包含了很多exception
            {
                Console.WriteLine("Database operation failed: "+ex.Message);
            }
            finally
            {
                Console.WriteLine("Press any key");
                Console.ReadKey();
            }


        }
    }
}
