﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10FirstEF
{
    class SocietyDbContext:DbContext
    {
        //public SocietyDbContext() : base("Day10FirstEF") { } 如果add db有问题， 就加这个"创建的db的名字"
        virtual public DbSet<Person> People { get; set; }
    }


}
