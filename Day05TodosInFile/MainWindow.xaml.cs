﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using System.Globalization;
using System.IO;
using Microsoft.Win32;
using System.Linq;

namespace Day05TodosInFile
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static List<Todo> todoList = new List<Todo>();
        public MainWindow()
        {
            InitializeComponent();
            loadFromFile();
            combo.ItemsSource = Enum.GetValues(typeof(StatusEnum));
            combo.SelectedIndex = 0;
            // DateTime myDate = DateTime.Now;
            // todoList.Add(new Todo( "buy milk",  4 ,myDate,StatusEnum.Delegated));
            lvTodo.ItemsSource = todoList;
        }

        const string FILEPATH = @"..\..\todos.txt";

        static void loadFromFile()
        {
            try
            {
                List<string> lines = new List<string>();
                lines = File.ReadAllLines(FILEPATH).ToList();

                foreach (string line in lines)
                {

                    Todo t = new Todo(line);
                    // string[] items = line.Split(';');
                    //Todo t = new Todo(items[0], Int32.Parse(items[1]), DateTime.Parse(items[2]), (StatusEnum)Enum.Parse(typeof(StatusEnum), items[3]));
                    todoList.Add(t);
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("I/O Error:" + ex.Message);
            }
        }
        void saveToFile()
        {
            try
            {
                using (StreamWriter fileOutput = new StreamWriter(FILEPATH))
                {
                    foreach (Todo t in todoList)
                    {
                        fileOutput.WriteLine(t);
                    }
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            }
        }
        void clearField()
        {
            tbTask.Clear();
            datePicker.Text = "";
            slider.Value = 4;
            combo.SelectedIndex = 0;
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string task = tbTask.Text;
                int diff = (int)slider.Value;
                if (datePicker.SelectedDate == null)
                {
                    MessageBox.Show("Select a due date first");
                    return;
                }
                // DateTime date = datePicker.SelectedDate.GetValueOrDefault();
                DateTime date = (DateTime)(datePicker.SelectedDate ?? null);
                StatusEnum status = (StatusEnum)combo.SelectedItem;
                Todo todo = new Todo(task, diff, date, status);
                todoList.Add(todo);
                lvTodo.Items.Refresh();
                clearField();
            }
            catch (InvalidValueException ex)
            {
                MessageBox.Show("input Error : " + ex.Message);
            }
        }


        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (lvTodo.SelectedIndex == -1)
            {
                MessageBox.Show("Select an item to update.");
                return;
            }
            Todo t = todoList[lvTodo.SelectedIndex];
            string task = tbTask.Text;
            int diff = (int)slider.Value;
            DateTime date = (DateTime)datePicker.SelectedDate;
            StatusEnum satus = (StatusEnum)combo.SelectedItem;
            //t=new Todo(task, diff, date, satus);
            t.Task = task;
            t.Difficulty = diff;
            t.DueDate = date;
            t.Status = satus;
            //todoList.Add(t);
            lvTodo.Items.Refresh();
            clearField();
        }
        private void lvTodo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvTodo.SelectedIndex >= 0)
            {
                Todo t = todoList[lvTodo.SelectedIndex];
                tbTask.Text = t.Task;
                tbDiff.Text = t.Difficulty + "";
                datePicker.Text = t.DueDate.ToString();
                combo.SelectedItem = t.Status;
            }
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lvTodo.SelectedIndex == -1)
            {
                MessageBox.Show("select an item to delete");
                return;
            }
            var result = MessageBox.Show("Are you sure to delete?", "confirm", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.No)
            {
                return;
            }
            todoList.RemoveAt(lvTodo.SelectedIndex);
            lvTodo.Items.Refresh();
            clearField();
        }

        private void mainWin_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            saveToFile();
        }

        private void btExport__Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.InitialDirectory = @"C:\Users\heshmat\Documents\dotnet\Day05TodosInFile";
            if (saveFileDialog.ShowDialog() == true)
                try
                {

                    using (StreamWriter fileOutput = new StreamWriter(saveFileDialog.FileName))
                    {
                        foreach (Todo t in todoList)
                        {
                            fileOutput.WriteLine(t);
                        }
                    }
                    MessageBox.Show("saves to file  " + saveFileDialog.SafeFileName);

                }
                catch (IOException ex)
                {
                    Console.WriteLine("Error reading file: " + ex.Message);
                }
        }
    }
    //*****************************
    public class InvalidValueException : Exception
    {
        public InvalidValueException(string msg) : base(msg) { }
    }
    public enum StatusEnum { Pending, Done, Delegated }


    //class Todo
    public class Todo
    {
        //constructor

        private string _task;
        public string Task
        {
            get
            {
                return _task;
            }
            set
            {
                Regex regex = new Regex("^[^;]{1,100}$");
                if (!regex.IsMatch(value))
                {
                    throw new InvalidValueException("task shoud be 1 to 100 character except ;");
                }
                _task = value;
            }
        }
        private int _difficulty;
        public int Difficulty //{ get; set; }
        {
            get
            {
                return _difficulty;
            }
            set
            {
                if (value < 1 || value > 5)
                {
                    throw new InvalidValueException("difficulty  shoud be 1 to 5  ;");
                }
                _difficulty = value;
            }

        }
        private DateTime _dueDate;
        public DateTime DueDate
        {
            get
            {
                return _dueDate;
            }
            set
            {
                if (value == null)
                {
                    throw new InvalidValueException("Date can't be null");
                }
                _dueDate = value;
            }
        }

        public StatusEnum Status { get; set; }

        public Todo(string task, int difficulty, DateTime dueDate, StatusEnum status)
        {
            Task = task;
            Difficulty = difficulty;
            DueDate = dueDate;
            Status = status;
        }

        public Todo(string dataLine)
        {
            try
            {
                string[] data = dataLine.Split(';');
                if (data.Length != 4)
                {
                    throw new InvalidValueException("Invalid line structure");
                }
                Task = data[0];
                Difficulty = int.Parse(data[1]); // ex
                DueDate = DateTime.Parse(data[2]); // ex
                Status = (StatusEnum)Enum.Parse(typeof(StatusEnum), data[3]);
            }
            catch (FormatException ex)
            {
                throw new InvalidDataException("Parsing error: " + ex.Message);
            }
            catch (ArgumentException ex)
            {
                throw new InvalidDataException("Parsing error: " + ex.Message);
            }
        }

        public override string ToString()
        {
            // string s = DueDate.ToShortDateString().ToString();
            return string.Format("{0};{1};{2};{3}", Task, Difficulty, DueDate, Status);
        }
    }
}
