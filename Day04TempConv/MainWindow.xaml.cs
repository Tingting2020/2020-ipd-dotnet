﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day04TempConv
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void radioButtonChecked(object sender, RoutedEventArgs e)
        {
            if (outputKelvin == null)
            { //UI not ready yet
                //Console.WrintLine("skipping");  在output可以看到这个执行了几次
                return;
            }
            Console.WriteLine("Convert temp triggered");  //看一下下面执行了几次

            string inputStr = tbInput.Text;
            double input;
            //设置了默认初始值的时候，没有输入input会报错，解决方法：
            if (inputStr == "")
            {
                lblOutput.Content = "No value entered";
                return;
            }

            if (!double.TryParse(this.tbInput.Text, out input))
            {
                MessageBox.Show("Input must be a number:\n");
            }

     

            /************inputCelcius************/
            if (inputCelcius.IsChecked == true)
            {
                if (outputCelcius.IsChecked == true)
                {
                    lblOutput.Content = input + "°C";
                   // lblOutput.Content = string.Format("{0:0.00}°C", input);  这个应该也可以
                }
                else if (outputFahrenheit.IsChecked == true)
                {
                    lblOutput.Content = (input * 1.8 + 32) + "°F";
                }
                else if (outputKelvin.IsChecked == true)
                {
                    lblOutput.Content = (input + 273.15) + "K";
                }
                else
                {
                    MessageBox.Show("Unable to find selected radio button", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            /************inputFahrenheit************/
            if (inputFahrenheit.IsChecked==true)
            {

                if (outputCelcius.IsChecked == true)
                {
                    lblOutput.Content = (input-32)*5/9 + "°C";
                }
                else if (outputFahrenheit.IsChecked == true)
                {
                    lblOutput.Content = input + "°F";
                }
                else if (outputKelvin.IsChecked == true)
                {
                    lblOutput.Content = string.Format("{0:0.00}", (input + 459.67) * 5 / 9) + "K";
                }
                else
                {
                    MessageBox.Show("Unable to find selected radio button", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            /************inputKelvin************/
            if (inputKelvin.IsChecked == true)
            {

                if (outputCelcius.IsChecked == true)
                {
                    lblOutput.Content = (input - 273.15) + "°C";
                }
                else if (outputFahrenheit.IsChecked == true)
                {
                    lblOutput.Content = (input-273.15)*9/5+32 + "°F";
                }
                else if (outputKelvin.IsChecked == true)
                {
                    lblOutput.Content = input +"K";
                }
                else
                {
                    MessageBox.Show("Unable to find selected radio button", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }

        }   

    }
}

/* 罗莹方法：没有单独设置同类型的，没有温度符号显示.  

       public void TempConvert()
        {
            double tempInput = 0;

            if (double.TryParse(tbInput.Text, out tempInput))
            {
                if (btInputKelvin.IsChecked == true && btOutputCelsius.IsChecked == true)
                {
                    tbOutput.Text = string.Format("{0:#.##} °C", (tempInput - 273.15));
                }
                else if (btInputKelvin.IsChecked == true && btOutputFah.IsChecked == true)
                {
                    tbOutput.Text = string.Format("{0:#.##} °F", (tempInput - 273.15) * 9 / 5 + 32);
                }
                else if (btInputFah.IsChecked == true && btOutputCelsius.IsChecked == true)
                {
                    tbOutput.Text = string.Format("{0:#.##} °C", (tempInput - 32) * 5 / 9);
                }
                else if (btInputFah.IsChecked == true && btOutputKelvin.IsChecked == true)
                {
                    tbOutput.Text = string.Format("{0:#.##} °K", (tempInput - 32) * 5 / 9 + 273.15);
                }
                else if (btInputCelsius.IsChecked == true && btOutputKelvin.IsChecked == true)
                {
                    tbOutput.Text = string.Format("{0:#.##} °K", tempInput + 273.15);
                }
                else if (btInputCelsius.IsChecked == true && btOutputFah.IsChecked == true)
                {
                    tbOutput.Text = string.Format("{0:#.##} °F", tempInput * 9 / 5 + 32);
                }
                else
                {
                    tbOutput.Text = tempInput.ToString();
                }
            }
        }

    
        //inputCelcius 按钮有：IsChecked="True" Checked="rdbt_Checked"
        private void rdbt_Checked(object sender, RoutedEventArgs e)
        {
            TempConvert();
        }
        //tbInput有TextChanged="tbInput_Changed"
        private void tbInput_Changed(object sender, TextChangedEventArgs e)   
        {
            TempConvert();
        } 
 
 */
