﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain_Alt2_Factory
{

	public class InvalidParameterException : Exception
	{
		public InvalidParameterException(string msg) : base(msg) { }
	}

	public class Person
	{
		// factory method  https://www.oodesign.com/  
		static public Person CreatePersonFromData(string dataLine)
		{
			string[] data = dataLine.Split(';');
			switch (data[0])
			{
				case "Person":
					{
						if (data.Length != 3)
						{
							throw new InvalidParameterException("Line has invalid number for fields:\n" + dataLine);
						}
						string name = data[1];
						int age;
						if (int.TryParse(data[2], out age))
						{
							throw new InvalidParameterException("Line age must be an integer value:\n" + dataLine);
						}
						return new Person(name, age); // ex
					}
				case "Student":
					{
						throw new NotImplementedException(); // for prototyping purposes only, FIXME  这个还没来得及写
					}
				case "Teacher":
					{
						if (data.Length != 5)
						{
							throw new InvalidParameterException("Line has invalid number for fields:\n" + dataLine);
						}
						string name = data[1];
						int age;
						if (int.TryParse(data[2], out age))
						{
							throw new InvalidParameterException("Line age must be an integer value:\n" + dataLine);
						}
						string subject = data[3];
						int yoe;
						if (int.TryParse(data[4], out yoe))
						{
							throw new InvalidParameterException("Years of experience must be an integer value:\n" + dataLine);
						}
						return new Teacher(name, age, subject, yoe); // ex
					}
				default:
					throw new InvalidParameterException("I don't know how to make:\n" + dataLine);
			}
		}


		private string _name;
		public string Name // 1-50 characters, no semicolons
		{
			get
			{
				return _name;
			}
			set
			{
				if (value.Length < 1 || value.Length > 50 || value.Contains(";"))
				{
					throw new InvalidParameterException("Name must be 1-50 characters long, no semicolons");
				}
			}
		}

		private int _age;
		public int Age // 0-150
		{
			get
			{
				return _age;
			}
			set
			{
				if (value < 0 || value > 150)
				{
					throw new InvalidParameterException("Age must be 0-150");
				}
			}
		}

		public Person(string name, int age)
		{
			Name = name;
			Age = age;
		}

		public override string ToString()
		{
			return string.Format("Person {0} is {1} y/o", Name, Age);
		}

	}

	public class Teacher : Person
	{
		public string Subject; // 1-50 characters, no semicolons
		public int YearsOfExperience; // 0-100

		public Teacher(string name, int age, string subject, int yoe) : base(name, age)
		{
			Subject = subject;
			YearsOfExperience = yoe;
		}
		public override string ToString()
		{
			return string.Format("Teacher {0} is {1} y/o, teaches {2} since {3} years", Name, Age, Subject, YearsOfExperience);
		}
	}

	public class Student : Person
	{
		public string Program; // 1-50 characters, no semicolons
		public double GPA; // 0-4.3
						   // Student(string dataLine) { ... }
		public Student(string name, int age, string program, double gpa) : base(name, age)
		{
			Program = program;
			GPA = gpa;
		}
		public override string ToString()
		{
			return string.Format("Student {0} is {1} y/o, studies {2} with {3} GPA", Name, Age, Program, GPA);
		}
	}


	class Program
	{
		static void Main(string[] args)
		{
			// Person p1 = new Person(); // not allowed because constructor is protected - it's a good thing


		}
	}

}
