﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day07HelloTwoWindows
{
    /// <summary>
    /// Interaction logic for HelloDialog.xaml
    /// </summary>
    public partial class HelloDialog : Window
    {
        //values retured to whoever instantiated this window

        // int age, double rand 两个参数的例子
        // public event Action<int,double> ResultAge; 这个最多可以传16个parameters

        public event Action<int> ResultAge;
        //value from constructor passed to this window
        //string name //only store if needed
        public HelloDialog(String name)
        {
            InitializeComponent();
            lblMessage.Content = string.Format("Hello {0},nice to meet you ",name);
        }

        private void ButtonOk_Click(object sender, RoutedEventArgs e)
        {
            int age = int.Parse(tbAge.Text);  //FIXME:ex
           /*两个参数的例子：
            double rand = new Random().NextDouble();
            AssignResult?.Invoke(age, rand); // execute call-back
           */

            /*
            if (ResultAge != null)
            {
                ResultAge(age);
            }*/
            ResultAge?.Invoke(age);  //execute call-back

            DialogResult = true;  //also closes the dialog
        }
    }
}
