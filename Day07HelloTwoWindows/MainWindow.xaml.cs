﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07HelloTwoWindows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
           // WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;  //设置main window center
        }

        private void ButtonSayHello_Click(object sender, RoutedEventArgs e)
        {
            int age=0;
            String name = tbName.Text;
            HelloDialog helloDialog = new HelloDialog(name);
            helloDialog.Owner = this;  //在helloDialog的xaml要设置-->WindowStartupLocation="CenterOwner"
            helloDialog.ResultAge += value => age = value;  //setup call-back  把value赋值给age,value是parameter

            bool? result = helloDialog.ShowDialog();  //bool只有true false, true? 有true false null三种结果
            Console.WriteLine("result is "+result);
            if (result == true)
            { //OK was clicked
                lblResult.Content = "Age is" + age;
            }


           /*两个参数的例子
            int ageVal = 0;
            double randVal = 0;

            String name = tbName.Text;
            HelloDialog helloDialog = new HelloDialog(name);
            helloDialog.Owner = this;
            helloDialog.AssignResult += (a, r) => { ageVal = a; randVal = r; };
            bool? result = helloDialog.ShowDialog();
            Console.WriteLine("Result is: " + result);
            if (result == true)
            { 
                lblResult.Content = string.Format("Age: {0}, Rand: {1}", ageVal, randVal);
            }
           */
        }
    }
}
