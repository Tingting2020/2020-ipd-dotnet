﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day07Sandwich
{
    /// <summary>
    /// Interaction logic for CustomDiag.xaml
    /// </summary>
    public partial class CustomDiag : Window
    {
        public event Action<string, string, string> AssignResult;

        public CustomDiag()
        {
            InitializeComponent();
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            string bread = comboBread.Text;
            List<string> vegList = new List<string>();
            if (cbVegLettuce.IsChecked == true)
            {
                vegList.Add("Lettuce");
            }
            if (cbVegTomatoes.IsChecked == true)
            {
                vegList.Add("Tomatoes");
            }
            if (cbVegCucumbers.IsChecked == true)
            {
                vegList.Add("Cucumbers");
            }
            string veggies = string.Join(", ", vegList);

            string meat = "";
            if (rbMeatChicken.IsChecked == true)
            {
                meat = "Chicken";
            }
            else if (rbMeatTurkey.IsChecked == true)
            {
                meat = "Turkey";
            }
            else if (rbMeatTofu.IsChecked == true)
            {
                meat = "Tofu";
            }
            else
            {   //internal error--invalid control flow
                MessageBox.Show("Unable to find selected radio button", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            //
            AssignResult?.Invoke(bread,veggies,meat);  //execute call-back
            DialogResult = true;  //also closes the dialog
        }
    }
}
