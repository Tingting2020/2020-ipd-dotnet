﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07Sandwich
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
       // string breadVal,vegVal,meatVal;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonMakeSandwich_Click(object sender, RoutedEventArgs e)
        {
            //instantiate show it as a modal dialog
            CustomDiag custDiag = new CustomDiag();
            //custDiag.Show();
            custDiag.Owner = this;
            custDiag.AssignResult += CustDlg_AssignResult;
            bool? result = custDiag.ShowDialog();
            //show it as modal dialog
        }

        private void CustDlg_AssignResult(string bread, string veg, string meat)
        {
            /*
            breadVal = bread;
            vegVal = veg;
            meatVal = meat;  用下面的方法也可以，用下面这个23行就不需要了
            */
            lblBread.Content = bread;
            lblVeggies.Content = veg;
            lblMeat.Content = meat;
        }
    }
}
