﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day04HelloWpf      //Namespace相当于Java的package，名字默认是project的名字，可以自己改
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window    //MainWindow继承Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_SayHelloViaLabelClick(object sender, RoutedEventArgs e)
        {
            string name=tbName.Text;
            // TODO: check name is not empty
            string greeting = string.Format("Hi {0}, nice to meet you!", name);
            lblGreeting.Content = greeting;
        }

        private void Button_SayHelloViaMsgBoxClick_1(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            // TODO: check name is not empty
            string greeting = string.Format("Hi {0}, nice to meet you!", name);
            MessageBox.Show(greeting);
        }

    }
}
