﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01PeopleListInFile
{
    class Person
    {
        public Person(string name, int age, string city) // : base()
        {
            Name = name;
            Age = age;
            City = city;
        }

        private string _name;
        public string Name  // Name 2-100 characters long, not containing semicolons
        {
            get
            {
                return _name;
            }
            set
            {
                // could use regex as well
                if (value.Length < 2 || value.Length > 100 || value.Contains(";"))
                {
                    throw new ArgumentException("Name length must be 2-100 characters");
                }
                _name = value;
            }
        }

        private int _age;
        public int Age  // Age 0-150
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new ArgumentException("Age must be 0-150");
                }
                _age = value;
            }
        }

        private string _city;
        public string City   // City 2-100 characters long, not containing semicolons
        {
            get
            {
                return _city;
            }
            set
            {
                // could use regex as well
                if (value.Length < 2 || value.Length > 100 || value.Contains(";"))
                {
                    throw new ArgumentException("City length must be 2-100 characters");
                }
                _city = value;
            }
        }

        public override string ToString()   //define a suitable ToString() method.
        {
            return String.Format("{0} is {1} y/o lives in {2}", Name, Age, City);
        }

    }

    class Program
    {
        static void AddPersonInfo()
        {
            Console.WriteLine("Adding a person.");
            Console.Write("Enter name: ");
            string name = Console.ReadLine();

            Console.Write("Enter age: ");
            string ageStr = Console.ReadLine();
            int age;
            if (!int.TryParse(ageStr, out age))
            {
                Console.WriteLine("Error: Age must be a valid integer.");
                return;
            }
            Console.Write("Enter city: ");
            string city = Console.ReadLine();
            try
            {
                Person person = new Person(name, age, city); // ex
                peopleList.Add(person);
                Console.WriteLine("Person added.");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }

        static void ListAllPersonsInfo()
        {
            /* foreach(Person p in peopleList)                  //call toString这部分
            {
                Console.WriteLine(p);
            } */
            Console.WriteLine(string.Join("\n", peopleList)); //注释的和这句两个方法都可以
        }


        static void FindPersonByName()
        {
            Console.Write("Please enter name your wnat to find:");
            string name = Console.ReadLine();
            if (name.Length < 2 || name.Length > 100 || name.Contains(";"))
            {
                Console.Write("The name must be 2-100 chars, not containing semicolons");
                return;
            }
            bool found = false;
            foreach (Person p in peopleList)
            {
                if (name == p.Name)
                {
                    found = true;
                    Console.WriteLine(p + "\n");
                }
            }
            if (!found)
            {
                Console.WriteLine("Did not find\n");
            }
        }


        static void FindPersonYoungerThan()
        {
            Console.Write("Please enter age:");
            string strAge = Console.ReadLine();

            if (!int.TryParse(strAge, out int age))
            {
                Console.Write("The age invalid.\n\n");
                return;
            }
            if (age < 0 || age > 150)
            {
                Console.Write("The age must be 0-150.\n\n");
                return;
            }
            bool found = false;
            foreach (Person p in peopleList)
            {
                if (p.Age <= age)
                {
                    found = true;
                    Console.WriteLine(p + "\n");
                }
            }
            if (!found)
            {
                Console.WriteLine("Did not find\n");
            }
        }

        static void LoadDataFromFile()
        {
               string[] presonLines = File.ReadAllLines(filename);
                for (int i = 0; i < presonLines.Length; i++)
                {
                    string[] preson = presonLines[i].Split(';');
                    try
                    {
                        Person p = new Person(preson[0], int.Parse(preson[1]), preson[2]);
                        peopleList.Add(p);
                        Console.WriteLine(p + "\n");                     
                    }
                    catch (InvalidOperationException ex)
                    {
                        Console.WriteLine("Data line invalid:" + presonLines[i].ToString() + "-->" + ex.Message + "\n");
                    }
            }
        }

        static void SaveDataToFile()
        {
            string strPeople = "";
            foreach (Person p in peopleList)
            {
                strPeople += p.Name + ";" + p.Age + ";" + p.City + "\n";
            }
            try
            {
                File.WriteAllText(filename, strPeople);
            }
            catch (Exception ex)
            {
                if (ex is SystemException || ex is IOException)
                {
                    Console.Write("fsdsf");
                }
                else
                {
                    throw ex;
                }
            }
        }

        private static int getMenuChoice()
        {
            while (true)
            {
                Console.Write(
    @"1. Add person info
2. List persons info
3. Find a person by name
4. Find all persons younger than age
0. Exit
Enter your choice: ");
                string choiceStr = Console.ReadLine();
                int choice;  //parse it to int and return it
                if (!int.TryParse(choiceStr, out choice) || choice < 0 || choice > 4)  //tryparse必须放在第一个，要先转换再有后面的验证
                {   //如果上面的不是只有||还有&&要注意优先级，或者tryparse之外的单独写一个if。 &&优先级高于||，  ！优先级更高些
                    Console.WriteLine("Value must be a number between 0-4");
                    continue;  //循环继续 再循环
                }
                return choice;
            }
        }

        static List<Person> peopleList = new List<Person>();
        const string filename = @"..\..\people.txt";

        static void Main(string[] args)
        { //At the start of the program you will read in all data from text file and load into 'people' list using the following method:
            LoadDataFromFile();
            while (true)
            {
                // show the menu and ask user's choice
                int choice = getMenuChoice();
                switch (choice)
                {
                    case 1:
                        AddPersonInfo();
                        break;
                    case 2:
                        ListAllPersonsInfo();
                        break;
                    case 3:
                        FindPersonByName();
                        break;
                    case 4:
                        FindPersonYoungerThan();
                        break;
                    case 0:
                        SaveDataToFile(); //before program exits you will save data from 'people' list back to the file
                        Console.WriteLine("Data saved. Exiting.");
                        System.Environment.Exit(-1);
                        break; // terminate the program
                    default:
                        Console.WriteLine("Internal error: invalid control flow in menu");
                        break;
                }
            }
        }
    }
}
