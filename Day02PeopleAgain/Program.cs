﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{

	public class InvalidParameterException : Exception
	{
		public InvalidParameterException(string msg) : base(msg) { }
	}


	class Program
	{
		static List<Person> peopleList;

		static void Main(string[] args)
		{
			Student st1 = new Student("Jerry", 33, "PhysEd", 3.8);
			st1.Name = "Jerry123";  //如果person里的Name是protected,这里也不能用。Name是internal或者public都可以

		}
	}

	public class Person
	{
		private string _name;
		public string Name // 1-50 characters, no semicolons       name,age是public才能被继承
		{
			get
			{
				return _name;
			}
			set
			{
				if (value.Length < 1 || value.Length > 50 || value.Contains(";"))
				{
					throw new InvalidParameterException("Name must be 1-50 characters long, no semicolons");
				}
			}
		}

		private int _age;
		public int Age // 0-150
		{
			get
			{
				return _age;
			}
			set
			{
				if (value < 0 || value > 150)
				{
					throw new InvalidParameterException("Age must be 0-150");
				}
			}
		}
		public Person(string dataLine)
		{
			string[] data = dataLine.Split(';');
			/* // can't do these verifications when constructor called from the constructor of a child class
			if (data.Length != 3)
            {
				throw new InvalidParameterException("Line has invalid number for fields:\n" + dataLine);
            }
			if (data[0] != "Person")             //因为child里没有“person”所以无法验证这步
            {
				throw new InvalidParameterException("Line does not define Person:\n" + dataLine);
			} */
			Name = data[1];
			int age;
			if (int.TryParse(data[2], out age))
			{
				throw new InvalidParameterException("Line age must be an integer value:\n" + dataLine);
			}
			Age = age;
		}

		public Person(string name, int age)
		{
			Name = name;
			Age = age;
		}

		public override string ToString()
		{
			return string.Format("Person {0} is {1} y/o", Name, Age);
		}

		public virtual string ToDataString()
		{
			return ""; // FIXME
		}
	}

	public class Teacher : Person
	{
		public string Subject; // 1-50 characters, no semicolons
		public int YearsOfExperience; // 0-100
		public Teacher(string dataLine) : base(dataLine)
		{
			string[] data = dataLine.Split(';');
			Subject = data[3];
			int yoe;
			if (int.TryParse(data[4], out yoe))
			{
				throw new InvalidParameterException("Years of experience must be an integer value:\n" + dataLine);
			}
			YearsOfExperience = yoe;
		}

		public Teacher(string name, int age, string subject, int yoe) : base(name, age)
		{
			Subject = subject;
			YearsOfExperience = yoe;
		}

		public override string ToString()
		{
			return string.Format("Teacher {0} is {1} y/o, teaches {2} since {3} years", Name, Age, Subject, YearsOfExperience);
		}
		public override string ToDataString()
		{
			return ""; // FIXME
		}

	}



	public class Student : Person
	{
		public string Program; // 1-50 characters, no semicolons
		public double GPA; // 0-4.3
						   // Student(string dataLine) { ... }
		public Student(string name, int age, string program, double gpa) : base(name, age)
		{
			Program = program;
			GPA = gpa;
		}
		public override string ToString()
		{
			return string.Format("Student {0} is {1} y/o, studies {2} with {3} GPA", Name, Age, Program, GPA);
		}
		public override string ToDataString()
		{
			return ""; // FIXME
		}

	}
}
