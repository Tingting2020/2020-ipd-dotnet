﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day04IceCreamSelector
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string currentItem = string.Empty;
        int index = 0;

        public MainWindow()
        {
            InitializeComponent();
            loadListBox();
        }

        static int flavourIndex, selectedIndex;

        private ArrayList loadListBox()
        {
            ArrayList list = new ArrayList();
            list.Add("Vanilla");
            list.Add("Chocolate");
            list.Add("Strawberry");
            list.Add("Peach");
            list.Add("Blueberry");
            list.Add("Mocha");
            lstFlavours.ItemsSource = list;
     
           return list;
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            if (lstFlavours.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please choose one from flavour to add");
            }
            else
            {
                currentItem = lstFlavours.SelectedValue.ToString();
                index = lstFlavours.SelectedIndex;
                //MessageBox.Show(currentItem);
                lstSelected.Items.Add(currentItem);
                //如果flavour在xaml里的list view item里，用下面的的方法
                //ListViewItem item = (ListViewItem)lstFlavours.SelectedItem;

                /* 罗莹的,她的加到selected就在左边lst里删掉了：
                 //single record-->
                //selectList.Add(list.ElementAt(flavourIndex));
                //flavourList.RemoveAt(flavourIndex);

                //multiple records-->
                var selectedFlavour = lstFlavours.SelectedItems;
                foreach (string s in selectedFlavour)
                {
                    selectList.Add(s);
                    list.Remove(s);
                }
                lstFlavours.Items.Refresh();
                lstSelected.Items.Refresh();                

                 */
            }
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            currentItem = lstSelected.SelectedValue.ToString();
            index = lstFlavours.SelectedIndex;
            lstSelected.Items.RemoveAt(lstSelected.Items.IndexOf(lstSelected.SelectedItem));
        }

        private void btClear_Click(object sender, RoutedEventArgs e)
        {
            lstSelected.Items.Clear();
        }

        private void lstFlavours_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            flavourIndex = lstFlavours.SelectedIndex;
        }

        private void lstSelected_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedIndex = lstSelected.SelectedIndex;
        }
    }
}
