﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day04ListView
{
    public partial class MainWindow : Window
    {
        List<Person> peopleList = new List<Person>();
        public MainWindow()
        {
            InitializeComponent();
            lvPerson.ItemsSource = peopleList;
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            string ageStr = tbAge.Text;
            int age;
            if(!int.TryParse(ageStr, out age))
            {
                MessageBox.Show("Age must be an integer");
                return;
            }
            Person person = new Person(name, age);
            //int retVal = lvPerson.Items.Add(person);
            peopleList.Add(person);
            lvPerson.Items.Refresh();
            ClearFields();
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (lvPerson.SelectedIndex == -1)
            {
                MessageBox.Show("Select a person to update");
                return;
            }
            Person person = peopleList[lvPerson.SelectedIndex];
            string ageStr = tbAge.Text;
            int age;
            if (!int.TryParse(ageStr, out age))
            {
                MessageBox.Show("Age must be an integer");
                return;
            }
            person.Name = tbName.Text;
            person.Age = age;
            lvPerson.Items.Refresh();
            ClearFields();
        }


        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lvPerson.SelectedIndex == -1)
            {
                MessageBox.Show("Select a person to delete");
                return;
            }
            Person person = (Person)lvPerson.SelectedItem;

            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure to delete " + person, "Delete Confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (messageBoxResult == MessageBoxResult.OK)
            {
                //Person p = (Person)lvDataBinding.SelectedValue;
                peopleList.Remove(person);
                lvPerson.Items.Refresh();
            }
        }

        private void ClearFields()
        {
            tbName.Text = "";
            tbAge.Text = "";
        }

        private void lvPerson_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvPerson.SelectedIndex == -1)
            {
                ClearFields();
                return;
            }
            Person person = (Person)lvPerson.SelectedItem;
            tbName.Text = person.Name;
            tbAge.Text = person.Age + "";
        }
    }

    class Person
    {
        // TODO: define toString and constructor
        public string Name { get; set; }

        private int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0)
                {
                    MessageBox.Show("Age can not be negative");
                }
                _age = value;
            }
        }
     public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public override string ToString()
        {
            string yrStr;
            if (Age == 1)
            {
                yrStr = "year";
            }
            else
            {
                yrStr = "years";
            }
            return string.Format("{0} is {1} {2} old", Name, Age, yrStr);
        }


    }
}

