﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10TodosEF
{
    public class Todo
    {
        // public Todo() : base() { } // parameterless constructor must exist or be auto-generated

        /* public Todo(string task) : base() {
            Task = task;
        } */
        // [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Task { get; set; }

        [Required]
        public int Difficulty { get; set; }

        public DateTime DueDate { get; set; }

        [EnumDataType(typeof(StatusEnum))]
        public StatusEnum Status { get; set; }
        public enum StatusEnum { Pending = 1, Done = 2, Delegated = 3 }
    }
}
