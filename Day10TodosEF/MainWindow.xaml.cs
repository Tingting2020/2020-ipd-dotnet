﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//Tools-->Nuget-->manager nuget-->browse 搜“entity framework" 110M那个，安装
//Tools-->Nuget-->Package Manager console-->在代码行粘：enable-migrations -EnableAutomaticMigrations:$true
//DB自动创建在 C: \Users\chenh\，  Data Connection里add connection选这个位置里的db

namespace Day10TodosEF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        TasksDbContext ctx;
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                ctx = new TasksDbContext();
                comboStatus.ItemsSource = Enum.GetValues(typeof(Todo.StatusEnum));
                ClearFieldsAndRefresh();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Fatal error connecting to database:\n" + ex.Message);
                Environment.Exit(1);
            }
        }


        private void ClearFieldsAndRefresh()
        {
            tbTask.Text = "";
            sliderDiff.Value = 2;
            dpDueDate.SelectedDate = DateTime.Now;
            comboStatus.SelectedIndex = -1;
            btnDelete.IsEnabled = false;
            btnUpdate.IsEnabled = false;
            lvTodosList.SelectedIndex = -1;
            List<Todo> todosList = (from t in ctx.Todos select t).ToList<Todo>();
            /* // detach all entities fetched - it may not be needed or even desired
            foreach (Todo t in todosList)
            {
                ctx.Entry(t).State = EntityState.Detached;
            }
            // */
            lvTodosList.ItemsSource = todosList;
            lvTodosList.Items.Refresh();
        }

        private bool VerifyFields()
        {
            List<String> errorsList = new List<string>();
            if (tbTask.Text.Length < 1 || tbTask.Text.Length > 100)
            {
                errorsList.Add("Task length must be 1 to 100 characters");
            }
            if (sliderDiff.Value < 1 || sliderDiff.Value > 5)
            {
                errorsList.Add("Difficulty must be 1 to 5");
            }
            if (dpDueDate.SelectedDate == null)
            {
                errorsList.Add("Due date must be selected");
            }
            if (comboStatus.SelectedIndex == -1)
            {
                errorsList.Add("Status must be selected");
            }
            if (errorsList.Count != 0)
            {
                MessageBox.Show(string.Join("\n", errorsList));
            }
            return (errorsList.Count == 0);
        }

        private void TodosList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Todo todo = (Todo)lvTodosList.SelectedItem;
            if (lvTodosList.SelectedIndex == -1)
            {
                ClearFieldsAndRefresh();
                return;
            }
            btnDelete.IsEnabled = true;
            btnUpdate.IsEnabled = true;
            tbTask.Text = todo.Task;
            sliderDiff.Value = todo.Difficulty;
            dpDueDate.SelectedDate = (DateTime)todo.DueDate;
            comboStatus.SelectedItem = todo.Status;
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!VerifyFields()) { return; }
            try
            {
                string task = tbTask.Text;
                int diff = (int)sliderDiff.Value;
                DateTime dueDate = (DateTime)dpDueDate.SelectedDate;
                Todo.StatusEnum status = (Todo.StatusEnum)comboStatus.SelectedItem;
                Todo t1 = new Todo { Task = task, Difficulty = diff, DueDate = dueDate, Status = status };
                ctx.Todos.Add(t1);
                ctx.SaveChanges();
                Console.WriteLine("New ID is: " + t1.Id);
                ClearFieldsAndRefresh();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database operation failed: " + ex.Message);
            }
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Todo t1 = (Todo)lvTodosList.SelectedItem;
                // Todo t1 = (from t in ctx.Todos where t.Id == todo.Id select t).FirstOrDefault<Todo>();
                if (t1 != null)
                {
                    ctx.Todos.Remove(t1);
                    ctx.SaveChanges();
                    ClearFieldsAndRefresh();
                }
                else
                {
                    MessageBox.Show("Unable to find record to delete");
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database operation failed: " + ex.Message);
            }
        }

        private void ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (!VerifyFields()) { return; }
            try
            {
                Todo t1 = (Todo)lvTodosList.SelectedItem;
                // Todo t1 = (from t in ctx.Todos where t.Id == todo.Id select t).FirstOrDefault<Todo>();
                if (t1 != null)
                {
                    t1.Task = tbTask.Text;
                    t1.Difficulty = (int)sliderDiff.Value;
                    t1.DueDate = (DateTime)dpDueDate.SelectedDate;
                    t1.Status = (Todo.StatusEnum)comboStatus.SelectedItem;
                    ctx.SaveChanges();
                    ClearFieldsAndRefresh();
                }
                else
                {
                    MessageBox.Show("Unable to find record to delete");
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Database operation failed: " + ex.Message);
            }
        }
    }
}
