﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*  
Declare:
String[] nameList = new nameList[5];

In a loop ask user for each name and put it into the array.
Ask user for a search string and save it in String 'search' variable;
In another loop find all names that contain the search string and display them.
In another loop find the longest name and display it.

Example session:
Enter a name: Jerry
Enter a name: Malexandra
Enter a name: Trixie
Enter a name: Tom
Enter a name: Barry

Enter search string: rr

Matching name: Jerry
Matching name: Barry

Longest name is: Malexandra

Your solution should NOT use sorting.
You are not allowed to use LINQ either.
You are not allowed to use List<>.

If there's more than one longest name only display the first one.
 */
namespace Day01NamesAgain
{
    class Program
    {
        static String[] nameList = new String[5];
        static void Main(string[] args)
        {
           try
            {     
                for (int i = 0; i < nameList.Length; i++)  //In a loop ask user for each name and put it into the array.
                {
                    Console.Write("Enter a name:");
                    nameList[i] = Console.ReadLine();
                }
                Console.Write("Enter search string:");  //Ask user for a search string and save it in String 'search' variable;
                String search = Console.ReadLine();
                foreach (String n in nameList)
                {
                    if (n.Contains(search))    //In another loop find all names that contain the search string and display them.
                    {
                        string line = string.Format("{0} contains {1} string", n, search);
                        Console.WriteLine(line);
                        //Console.WriteLine("{0} contains {1} string", n, search);
                    }
                }

                //String longest=""; 赋值，用下面的方法更常见
                String longest = nameList[0];
                foreach (String n in nameList)
                {
                    if (longest.Length < n.Length)    //In another loop find the longest name and display it.
                    {
                        longest = n;            //If there's more than one longest name only display the first one.
                    }
                }
                Console.WriteLine("The longest is " + longest);
            }
            finally
            {
                Console.WriteLine("Press any key");
                Console.ReadKey();
            }
        }
    }
}
