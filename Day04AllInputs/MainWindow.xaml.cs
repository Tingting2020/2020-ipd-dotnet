﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day04AllInputs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_RegisterMeClick(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;  //check name empty???


            string age = "";
            if (rbAgeBelow18.IsChecked==true)
            {
                age = "below18";
            }else if (rbAge18to35.IsChecked == true)
            {
                age = "18 to 35";
            }else if (rbAge35andUp.IsChecked == true)
            {
                age = "36 and up";
            }
            else
            {
                MessageBox.Show("Unable to find selected radio button", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
            }

            var petsList = new ArrayList();

            if (cbPetCat.IsChecked == true)
            {
                petsList.Add("cats");
            }
             if(cbPetDog.IsChecked == true)
            {
                petsList.Add("dogs");
            }
            if(cbPetOther.IsChecked == true)
            {
                petsList.Add("other");
            }
            string pets = string.Join(",", petsList.ToArray());
            string continent = comboContinent.Text;
            string temp = tbSlider.Text;  //data format???
            //lblTemp.Content = string.Format("{0:0.00}", sliderTemp.Value);  设置format，{0:0}是int
            //lblTemp.Content=sliderTemp.Value+";

            string result = string.Format("Hi {0}, age{1}, pet:{2},{3},temp: {4},nice to meet you!", name, age, pets, continent,temp);
            lblResult.Content = result;

            /*
                      //java printwriter append 用FileWriter(new File("output.txt"), true 部分实现输入多条语句到文件
                      try (PrintWriter fileOutput = new PrintWriter(new FileWriter(new File("output.txt"), true))) {
                          fileOutput.println(data);
                          JOptionPane.showMessageDialog(this, "Data written",    //数据都输入了，提示用户成功写入
                                  "Success", JOptionPane.INFORMATION_MESSAGE);
                      } catch (IOException ex)
                      {
                          JOptionPane.showMessageDialog(this, "Error writing to file:\n" + ex.getMessage(),
                                  "File access error", JOptionPane.ERROR_MESSAGE);
                      }
          */
        }
    }
}
