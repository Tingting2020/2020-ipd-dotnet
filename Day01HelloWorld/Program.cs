﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01HelloWorld
{
    class Program                   //没有public， 在这个project的也都可以调用
    {
        static void Main(string[] args)
        {
            try
            {
                Console.Write("What is your name? ");
                string name = Console.ReadLine();
                Console.Write("How old are you? ");
                string ageStr = Console.ReadLine();

                // int age = Convert.ToInt32(Console.ReadLine()); 这个要用try catch，因为会有2种exception出现

                int age;
                if (!int.TryParse(ageStr, out age))   //out的用法:需要return 1个以上的value时用。如果true而且会直接改掉age本身的值
                {
                    Console.WriteLine("Error: input must be an integer, age was parsed to {0}", age);
                    //Environment.Exit(1);
                    return;
                }
                Console.WriteLine("Hello {0}, you are {1} y/o, nice to meet you {0}!", name, age);
            }
            finally
            {
                Console.WriteLine("Press any key to finish");
                Console.ReadKey();
            }

     

            /*
            try
            {
                int age = int.Parse(ageStr);
                Console.WriteLine("Hello {0} ,you are {1} years old, nice to meet you {0}! ", name, age);
            }
            catch (System.FormatException ex)
            {
                Console.WriteLine("Input must be an integer!"+ex.Message());
            }
            */


            // Console.WriteLine("Hello " + name+ ", you are "+age+" years old, nice to meet you!"); 用下面的格式类似java format
           // Console.WriteLine("Hello {0} ,you are {1} years old, nice to meet you {0}! ",name,age);

            // Console.WriteLine("Hello World!");

           // Console.WriteLine("Press any key to finish");
          //  Console.ReadKey();
        }
    }
}

//private field和local variables, parameter是小写，其他都是大写开头
//long是int64的alias在.net里，
//kiss= keep it simple, stupid
/*            int x;
            long y;
            short z;
            string n;
            bool b;
            char c;*/

//.net都是unchecked exception 所以即便有exception只会crash死机崩溃。但是network io, file, database,api要考虑exception的问题