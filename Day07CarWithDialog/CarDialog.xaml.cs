﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day07CarWithDialog
{
    /// <summary>
    /// Interaction logic for CarDialog.xaml
    /// </summary>
    public partial class CarDialog : Window
    {
        Car currCar; // null when adding, non-null when editing
        public event Action<Car> AddNewCarCallback;
        public CarDialog(Car car)
        {
            InitializeComponent();
            comboFuelType.ItemsSource = Enum.GetValues(typeof(Car.FuelTypeEnum));
            comboFuelType.SelectedIndex = 0;

            if (car != null) // Double click to update
            {
                currCar = car;
                tbMakeModel.Text = car.MakeModel;
                sliderEngineSize.Value = car.EngineSize;
                comboFuelType.SelectedItem = car.FuelType;
                btnSaveUpdate.Content = "Update";
            }
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            string makeModel = tbMakeModel.Text;
            double engineSize = sliderEngineSize.Value;
            Car.FuelTypeEnum fuelType = (Car.FuelTypeEnum)comboFuelType.SelectedItem;

            try
            {
                if (currCar != null) // update
                {
                    currCar.MakeModel = makeModel; // ex
                    currCar.EngineSize = engineSize; // ex
                    currCar.FuelType = fuelType; // ex
                } else
                { // assign result only on add
                    AddNewCarCallback?.Invoke(new Car(makeModel, engineSize, fuelType));
                }
                DialogResult = true;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Errors found", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }
    }
}
