﻿using CsvHelper;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07CarWithDialog
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static List<Car> CarList = new List<Car>();
        const string fileName = @"..\..\cars.txt";
        public MainWindow()
        {
            InitializeComponent();
            ((INotifyCollectionChanged)lvCars.Items).CollectionChanged += lvCars_CollectionChanged;
            LoadDataFromFile();
            lvCars.ItemsSource = CarList;
            lvCars.Items.Refresh();
        }
        private void lvCars_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            tbStatus.Text = $"You have {CarList.Count} car(s) currently";
        }

        static void SaveDataToFile()
        {
            try
            {
                using (TextWriter tw = new StreamWriter(fileName, false))
                {
                    foreach (var c in CarList)
                    {
                        tw.WriteLine(c.ToDataString());
                    }
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error writing file: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        static void LoadDataFromFile()
        {
            try
            {
                using (StreamReader file = new StreamReader(fileName))
                {
                    List<string> errorsList = new List<string>();
                    int counter = 0;
                    string ln;
                    while ((ln = file.ReadLine()) != null)
                    {
                        try
                        {
                            counter++;
                            CarList.Add(new Car(ln));
                        }
                        catch (ArgumentException ex)
                        {
                            errorsList.Add(ex.Message + " in line " + counter + "\n=> " + ln);
                            continue;
                        }
                    }
                    if (errorsList.Count != 0)
                    {
                        MessageBox.Show(String.Join("\n", errorsList), "Errors found", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                // ignore it - it's ok
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error reading file: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void MenuItemAdd_Click(object sender, RoutedEventArgs e)
        {
            CarDialog carDialog = new CarDialog(null) { Owner = this };
            carDialog.AddNewCarCallback += (newCar) => { CarList.Add(newCar); };
            carDialog.ShowDialog();
            lvCars.Items.Refresh();
        }

        private void EditItemCurrSelected()
        {
            Car car = (Car)lvCars.SelectedItem;
            if (car == null) { return; }
            CarDialog carDialog = new CarDialog(car) { Owner = this };
            carDialog.ShowDialog();
            lvCars.Items.Refresh();
        }

        private void lvCars_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            EditItemCurrSelected();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveDataToFile();
        }

        private void MenuItemExportToCsv_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "CSV file (*.csv)|*.csv";
            saveFileDialog.Title = "Export to file";
            if (saveFileDialog.ShowDialog() == true)
            {
                try
                {
                    using (var writer = new StreamWriter(saveFileDialog.FileName))
                    using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                    {
                        csv.Configuration.Delimiter = ";";
                        csv.WriteRecords(CarList);
                    }
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error writing file: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void MenuItemExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void MenuItemDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lvCars.SelectedIndex == -1)
            {
                MessageBox.Show("Select item to delete", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            CarList.RemoveAt(lvCars.SelectedIndex);
            lvCars.Items.Refresh();
        }

        private void MenuItemEdit_Click(object sender, RoutedEventArgs e)
        {
            EditItemCurrSelected();
        }
    }
}
