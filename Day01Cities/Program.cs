﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01Cities
{
    class City
    {
        public string Name;       //field(Name) in C# by default is private,所以想访问可以改为public string Name ， 
        //internal string Name;   //internal是指在同一个project(Assembly即library在references里那些)里可以访问，一个solution里是可以建很多project的
        public double PopulationMillions;
        public override string ToString()
        {
            return string.Format("City: {0} with {1} mil. population", Name, PopulationMillions);

        }
    }

    class BetterCity  //练习getter setter
    {
        public BetterCity(int id, string name)  //constructor
        {
            Id = id;
            // 不要写_name2 = name; // almost always the WRONG thing to do
            Name2 = name;         //call setter
        }

        public int Id; // field
        public string Name { get; set; } // property with storage and default getter/setter 这个简易的写法也是自带get set的，只是没有验证功能
        private string _name2; // storage for a property此处是Name2的值存在这里。private是在BetterCity里的可以访问，其他不可以
           //_name2 :名字加_是自己为了区别这个只是用在getter setter中，不是普通变量，不能随便调用
        // property  
        public string Name2 // no storage, just getter and setter  此处的Name2是property，property里可以只有一个getter或者一个setter
        {   //property里面只能有get set不能定义一个新的变量
            get
            {
                return _name2;
            }
            set
            {
                if (value.Length < 2)         //value类型和上面定义的property(Name2)类型一样的，是string在这里
                {
                    throw new InvalidOperationException("Name length must be at least 2 characters");
                }
                _name2 = value;
            }
        }
    }   //31行的getter setter和32+35行的，是一样的。用哪个都可以


    class Program
    {                           //注意CitiesList是大写开始
        static public List<City> CitiesList = new List<City>();  //C#里array list要写上type(City) java不用

        static void Main(string[] args)
        {
            try
            {
                City c1 = new City();
                c1.Name = "Montreal";
                c1.PopulationMillions = 2.5;
                Console.WriteLine(c1);

                City c2 = new City { Name = "Toronto", PopulationMillions = 4.5 }; //同于61-64行，给field赋值 "object initializer"

                BetterCity bc1 = new BetterCity(1, "SomeCity");
                bc1.Name = "Vancouver";
                bc1.Name2 = "VA"; // ex 此处调用了Name2-> 47行的setter，把VA赋值给Value变量。
                String nnn = bc1.Name2;   //此处调用了Name2的getter

                CitiesList.Add(new City() { Name = "New York", PopulationMillions = 8 });
                CitiesList.Add(new City() { Name = "LA" });  //PopulationMillions不赋值的话就是默认值
                CitiesList.Add(new City() { Name = "London" });

                CitiesList[1].PopulationMillions = 10;//array list没有get set这个[1]是indexer,此处赋值给"LA" 这个相当于set

                double pop2 = CitiesList[2].PopulationMillions;  //这个相当于get 把"London"的值付给pop2

                foreach (City c in CitiesList)     //loop
                {
                    Console.WriteLine("City is: " + c); //有toString所以此处可以运行正常，没有的话只会print namespace和name of class
                }   //没有toString会打印“City is:Day01Cities.City”

            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            finally
            {
                Console.WriteLine("Press any key");
                Console.ReadKey();
            }
        }
    }
}
//名字都大写，除了local variable--> 61行的c1,  parameters-->57行的args,  private field-->32行的_name2