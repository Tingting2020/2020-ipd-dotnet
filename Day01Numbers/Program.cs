﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
Ask user how many numbers he/she wants to generate.
Generate the numbers as random integers in -100 to +100 range, both inclusive.
Place the numbers in List<int> named numList.

In a next loop find the numbers that are less or equal to 0 and print them out, one per line.
*/
namespace Day01Numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> numList = new List<int>();
            while (true)
            {
                Console.Write("How many numbers you want to generate? ");
                if (!int.TryParse(Console.ReadLine(), out int num))
                {
                    Console.WriteLine("Invalid number\n");
                    continue;
                }
                Random random = new Random();
                for (int i = 0; i < num; i++)
                {
                    int n = random.Next(-100, 100);
                    numList.Add(n);
                }
                Console.WriteLine("Here is the random integers generate: ");
                Console.WriteLine(string.Join(", ", numList.ToArray()));

                List<int> resultNum = numList.FindAll(x => x <= 0);
                Console.WriteLine("All the numbers less or equal to 0: ");
                Console.WriteLine(string.Join("\n", resultNum.ToArray()));

                Console.WriteLine("press any key....");
                Console.ReadKey();
                break;
            }         
            }
        }
    }

