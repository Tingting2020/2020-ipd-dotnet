﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01NameAgainUseDelegate
{       //委托（Delegate） 就是pass a function as a parameter
    class Program
    {
        public delegate void ReportingDelegateType(string message);  //ReportingDelegateType是一个delegate type 起这个名字是为了我们明白

        static void reportFindingToFile(string msg)
        {   //append就是把每次运行的结果都存在里面，不会丢失
            // FIXME: unhandled ioexception
            File.AppendAllText(@"..\..\log.txt", "FOUND: " + msg + "\n");   //@用来parse to file,加了两个..\往上两级文件夹
           // File.AppendAllText("log.txt", "FOUND: " + msg + "\n"); 如果路径只写这个，存在bin/debug下面
            //File.ReadAllLines(); 非常有用的功能
            //File.WriteAllLines(); 非常有用的功能
        }

        static void reportFindingOnScreen(string msg)
        {
            Console.WriteLine("FOUND: " + msg);
        }


        static string[] nameList = new string[5];

        static void Main(string[] args)
        {
            // delegate variable is a list of references to methods
            ReportingDelegateType report = null;

            Random random = new Random();
            //Random的决定把2个方法中的哪个赋给report，有时是一个，有时是2个，有时可能都没有
            if (random.Next(0, 2) == 1)
            {
                Console.WriteLine("Adding reporting on screen");
                report += reportFindingOnScreen;  //把reportFindingOnScreen方法赋值给变量report，后面可以只用report调用这个方法
            }
            if (random.Next(0, 2) == 1)
            {
                Console.WriteLine("Adding reporting to file");
                report += reportFindingToFile;
            }

            try
            {
                for (int i = 0; i < nameList.Length; i++)
                {
                    Console.Write("Enter a name: ");
                    nameList[i] = Console.ReadLine();
                }

                Console.Write("Enter search string: ");
                String search = Console.ReadLine();
                foreach (String n in nameList)
                {
                    if (n.Contains(search))
                    {
                        string line = string.Format("{0} contains {1} string", n, search);
                        Console.WriteLine(line);
                        /*
                        if(report!=null)
                        {
                         report(line);  
                        }
                        */
                        report?.Invoke(line); // call only if not null 这个和上面注释掉的一样的功能，是简写版  Invoke调用
                    }
                }

                // reference can also be removed from the list of delegate variable
                // report -= reportFindingToFile;   把reportFindingToFile方法从report里remove

                // String longest = "";
                String longest = nameList[0];
                foreach (String n in nameList)
                {
                    if (longest.Length < n.Length)
                    {
                        longest = n;
                    }
                }
                Console.WriteLine("The longest is " + longest);
            }
            finally
            {
                Console.WriteLine("Press any key");
                Console.ReadKey();
            }
        }
    }
}