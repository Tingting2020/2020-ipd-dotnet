﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace Day05Notepad
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string CurrentFile = "";
        bool IsModified = false;

        public MainWindow()
        {
            InitializeComponent();
            lblFileName.Text = "No File Open";
        }

        private void miOpen_Click(object sender, RoutedEventArgs e)
        {
            if (IsModified)
            {
                MessageBox.Show("Please save first");
                miSave_Click(sender, e);
            }

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";   //设置文件格式
            //openFileDialog.InitialDirectory = @"C:\Users\chenh\Documents\Vanier";   //设置默认打开的路径
            if (openFileDialog.ShowDialog() == true)
            {
                CurrentFile = openFileDialog.FileName;
                tbEditor.Text = File.ReadAllText(CurrentFile);
                Title = System.IO.Path.GetFileName(CurrentFile);
                lblFileName.Text = System.IO.Path.GetDirectoryName(CurrentFile);
            }

        }

        private void miSave_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            if (CurrentFile != "")
            {
                string content = tbEditor.Text;
                File.WriteAllText(CurrentFile, content);
                IsModified = false;
                updateStatus();
            }
            else
            {
                miSaveAs_Click(sender, e);
            }
        }

        private void miSaveAs_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == true)
            {
                CurrentFile = saveFileDialog.FileName;
                File.WriteAllText(CurrentFile, tbEditor.Text);
                IsModified = false;
                updateStatus();
                // Title = System.IO.Path.GetFileName(CurrentFile);
                // lblFileName.Text = System.IO.Path.GetFileName(CurrentFile);
            }

            //saveFileDialog.InitialDirectory = @"c:\temp\";   //设置默认保存的路径
            //saveFileDialog.Filter = "Text file (*.txt)|*.txt|C# file (*.cs)|*.cs";

        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
/* 如何显示文件名?
         MessageBoxResult result = MessageBox.Show("Would you like to save \"Hello, world\"?", "My App", MessageBoxButton.YesNoCancel);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    MessageBox.Show("Hello to you too!", "My App");
                    break;
                case MessageBoxResult.No:
                    MessageBox.Show("Oh well, too bad!", "My App");
                    break;
                case MessageBoxResult.Cancel:
                    MessageBox.Show("Nevermind then...", "My App");
                    break;
            }*/
            // Environment.Exit(0);
            //App.Current.MainWindow.Close();
            Close();
        }

        private void tbEditor_TextChanged(object sender, TextChangedEventArgs e)
        {
            /*
            int row = tbEditor.GetLineIndexFromCharacterIndex(tbEditor.CaretIndex);
            int col = tbEditor.CaretIndex - tbEditor.GetCharacterIndexFromLineIndex(row);
            lblCursorPosition.Text = "Line " + (row + 1) + ", Char " + (col + 1);*/
            IsModified = true;
            updateStatus();
        }

        private void updateStatus()
        {
            Title = (CurrentFile == "" ? "No file open" : System.IO.Path.GetFileName(CurrentFile)) + (IsModified ? " (modified)" : "");
            lblFileName.Text = (CurrentFile == "" ? "No file open" : CurrentFile);
        }
    }
}
