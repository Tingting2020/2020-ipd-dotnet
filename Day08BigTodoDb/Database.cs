﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TodoList
{
    public class Database
    {
        const string ConnString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\chenh\Documents\2020-ipd-dotnet\Day08BigTodoDb\Day08BigTodoDb.mdf;Integrated Security=True;Connect Timeout=30";
        private SqlConnection conn;
        public Database() // ex
        {
            conn = new SqlConnection(ConnString);
            conn.Open(); // ex
        }

        public enum SortOrder { Task, DueDate }

        public List<Todo> GetAllTodos(SortOrder order /* = SortOrder.Task */) // SqlException, InvalidValueException
        {
            List<Todo> list = new List<Todo>();
            using (SqlCommand cmd = new SqlCommand("SELECT * FROM Todos ORDER BY " + order, conn))
            using (SqlDataReader reader = cmd.ExecuteReader()) // ex
            {
                while (reader.Read())
                {
                    list.Add(new Todo(reader)); // SqlException, InvalidValueException
                }
            }
            return list;
        }

        public void AddTodo(Todo todo) // ex
        {
            using (SqlCommand cmd = new SqlCommand("INSERT INTO Todos (Task, DueDate, TaskStatus) VALUES (@Task, @DueDate, @TaskStatus)", conn))
            {
                cmd.Parameters.AddWithValue("@Task", todo.Task);
                cmd.Parameters.AddWithValue("@DueDate", todo.DueDate);
                string status = todo.TaskStatus.ToString();
                cmd.Parameters.AddWithValue("@TaskStatus", status);
                cmd.ExecuteNonQuery(); // ex
            }
        }

        public void UpdateTodo(Todo todo)
        {
            using (SqlCommand cmd = new SqlCommand("UPDATE Todos SET Task = @Task, DueDate = @DueDate, TaskStatus = @TaskStatus WHERE Id=@Id", conn))
            {
                cmd.Parameters.AddWithValue("@Task", todo.Task);
                cmd.Parameters.AddWithValue("@DueDate", todo.DueDate);
                string status = todo.TaskStatus.ToString();
                cmd.Parameters.AddWithValue("@TaskStatus", status);
                cmd.Parameters.AddWithValue("@Id", todo.Id);
                cmd.ExecuteNonQuery();
            }
        }

        public void DeleteTodo(int id)
        {
            using (SqlCommand cmd = new SqlCommand("DELETE FROM Todos WHERE Id=@Id", conn))
            {
                cmd.Parameters.AddWithValue("@Id", id);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
