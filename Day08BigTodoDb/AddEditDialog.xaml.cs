﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TodoList
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        Todo selectedTodo; // this will be null for adding and not null for updating
        
        public AddEditDialog(Todo todo)
        {
            InitializeComponent();
            if (todo != null) //for updating to initiate the dialog with selected object
            {
                selectedTodo = todo;
                lblId.Content = todo.Id + string.Empty;
                tbTask.Text = todo.Task;
                dpDueDate.SelectedDate = todo.DueDate;
                cbStatus.IsChecked = todo.TaskStatus == TaskStatusEnum.Done ? true : false;
                btSave.Content = "Update";
            }
        }
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (selectedTodo != null) // for updating a Task
                {
                    selectedTodo.Task = tbTask.Text; //ex
                    selectedTodo.DueDate = (DateTime)(dpDueDate.SelectedDate ?? null);  // ex
                    selectedTodo.TaskStatus = cbStatus.IsChecked == true ? TaskStatusEnum.Done : TaskStatusEnum.Pending;
                    Globals.db.UpdateTodo(selectedTodo);
                } else if (string.IsNullOrEmpty(tbTask.Text) || dpDueDate.SelectedDate.Equals(null))
                {
                    MessageBox.Show("Please input value of task and due date", "Information");
                    return;
                }else
                {// for adding a Task
                    string task = tbTask.Text; //ex
                    DateTime dueDate = (DateTime)(dpDueDate.SelectedDate ?? null);//ex
                    TaskStatusEnum status = cbStatus.IsChecked == true ? TaskStatusEnum.Done : TaskStatusEnum.Pending;
                    Todo todo = new Todo(0, task, dueDate, status);
                    Globals.db.AddTodo(todo);
                }
                DialogResult = true;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            catch (InvalidValueException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
