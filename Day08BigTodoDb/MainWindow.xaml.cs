﻿using CsvHelper;
using CsvHelper.TypeConversion;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TodoList
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Todo> todosList = new List<Todo>();
        Database.SortOrder currSortOrder = Database.SortOrder.Task;
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                Globals.db = new Database();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("FATAL Database error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                Environment.Exit(1);
            }
            refreshAndStatus("Data loaded");
        }

        private void refreshAndStatus(string msg)
        {
            try
            {
                if (Globals.db == null) { return; } // database not connected yet
                todosList = Globals.db.GetAllTodos(currSortOrder); // SqlException, InvalidValueException
                lvTodos.ItemsSource = todosList;
                tbStatus.Text = msg;
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Database error: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            catch (InvalidValueException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            AddEditDialog addEditdlg = new AddEditDialog(null) { Owner = this };
            if (addEditdlg.ShowDialog() == true)
            {
                refreshAndStatus("Record added");
            }
        }

        private void miDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lvTodos.SelectedIndex == -1)
            {
                MessageBox.Show("Select item to delete", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            Todo todo = (Todo)lvTodos.SelectedItem;

            if (MessageBox.Show($"Do you want to delete {todo.Task}", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
            {
                return;
            }
            Globals.db.DeleteTodo(todo.Id);
            refreshAndStatus("Record deleted");
        }

        private void lvTodos_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Todo todo = (Todo)lvTodos.SelectedItem;
            if (todo == null) { return; }
            AddEditDialog addEditDlg = new AddEditDialog(todo) { Owner = this };
            if (addEditDlg.ShowDialog() == true)
            {
                refreshAndStatus("Record updated");
            }
        }

        private void ExportToCSV_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog exportDialog = new SaveFileDialog();
            exportDialog.Filter = "CSV file (*.csv)|*.csv";
            exportDialog.Title = "Export to file";
            if (exportDialog.ShowDialog() == true)
            {
                try
                {
                    using (StreamWriter writer = new StreamWriter(exportDialog.FileName))
                    using (CsvWriter csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                    {
                        csv.Configuration.Delimiter = ";";
                        var options = new TypeConverterOptions { Formats = new[] { "yyyy-MM-dd" } };
                        csv.Configuration.TypeConverterOptionsCache.AddOptions<DateTime>(options);
                        csv.WriteRecords(todosList);
                    }
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error exporting to csv: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Task_Checked(object sender, RoutedEventArgs e)
        {
            currSortOrder = Database.SortOrder.Task;
            refreshAndStatus("Sorted by Task");
        }

        private void DueDate_Checked(object sender, RoutedEventArgs e)
        {
            currSortOrder = Database.SortOrder.DueDate;
            refreshAndStatus("Sorted by Due Date");
        }

    }
}
