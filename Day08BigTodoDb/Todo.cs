﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace TodoList
{
    public class Todo
    {
        public int Id { get; set; }

        private string _task;
        public string Task
        {
            get
            {
                return _task;
            }
            set
            {
                Regex regex = new Regex("^[^;]{1,50}$");
                if (!regex.IsMatch(value))
                {
                    throw new InvalidValueException("Task shoud be 1 to 50 character except ;");
                }
                _task = value;
            }
        }

        private DateTime _dueDate;
        public DateTime DueDate
        {
            get
            {
                return _dueDate;
            }
            set
            {
                if (value == null)
                {
                    throw new InvalidValueException("Date can't be null");
                }
                _dueDate = value;
            }
        }

        public TaskStatusEnum TaskStatus { get; set; }

        public Todo(int id, string task, DateTime dueDate, TaskStatusEnum status)
        {
            Id = id;
            Task = task;
            DueDate = dueDate;
            TaskStatus = status;
        }

        public Todo(SqlDataReader reader) // SqlException, InvalidValueException,
        {
                Id = reader.GetInt32(reader.GetOrdinal("Id"));
                Task = reader.GetString(reader.GetOrdinal("Task"));
                DueDate = reader.GetDateTime(reader.GetOrdinal("DueDate"));
                string taskStatusStr = reader.GetString(reader.GetOrdinal("TaskStatus"));
                if (!Enum.TryParse<TaskStatusEnum>(taskStatusStr, out TaskStatusEnum tStatus))
                {
                    throw new InvalidValueException("Cannot parse Task Status");
                }
                TaskStatus = tStatus;
        }
        public override string ToString()
        {
            // string s = DueDate.ToShortDateString().ToString();
            return string.Format("{0};{1};{2};{3}", Id, Task, DueDate, TaskStatus);
        }
    }
    public enum TaskStatusEnum { Done, Pending }
}
