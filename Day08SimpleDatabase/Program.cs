﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day08SimpleDatabase
{
    class Program
    {
        //点击data connection里的db，右侧Properties里的connection string,  @一定要加上
        const string ConnString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\chenh\Documents\2020-ipd-dotnet\Day08SimpleDatabase\SimpleDb.mdf;Integrated Security=True;Connect Timeout=30";
        static void Main(string[] args)
        {
            try
            {
                SqlConnection conn = new SqlConnection(ConnString);
                conn.Open(); //ex

                Random random = new Random();

/************************************************************************************************************/
                {   //insert   建议sql的保留字用大写 ,用select的时候要有using(){}把内容括起来，不用select的话可以不用加
                    using (SqlCommand cmd = new SqlCommand("INSERT INTO People(Name,Age) VALUES(@Name,@Age)", conn))
                    {
                        cmd.Parameters.AddWithValue("@Name", "Jerry" + random.Next());
                        cmd.Parameters.AddWithValue("@Age", random.Next(1, 100));
                        cmd.ExecuteNonQuery();   //ex
                    }
                }
/************************************************************************************************************/
                {  //select 
                    using (SqlCommand cmd = new SqlCommand("SELECT * FROM People", conn))
                    using (SqlDataReader reader=cmd.ExecuteReader())  //ex
                    {
                        while (reader.Read())
                        {
                            //int id = (int)reader["Id"];
                            //int id = reader.GetInt32(0);
                            int id = reader.GetInt32(reader.GetOrdinal("Id"));  //GetByte会返回tinyint，GetInt64返回long
                            string name = reader.GetString(reader.GetOrdinal("Name"));
                            int age = reader.GetInt32(reader.GetOrdinal("Age"));
                            Console.WriteLine("{0}: {1} is {2} y/o", id, name, age);
                        }
                    }
                }
                /************************************************************************************************************/
                {  //select specific record
                    int wantId = 3; //looking for this one record
                    Console.WriteLine("Looking for record with Id=" + wantId);
                    using (SqlCommand cmd = new SqlCommand("SELECT * FROM People WHERE Id=@Id", conn))
                    {
                        cmd.Parameters.AddWithValue("@Id", wantId);
                        using (SqlDataReader reader = cmd.ExecuteReader()) // ex
                        {
                            if (reader.Read())
                            {
                                //int id = (int)reader["Id"];
                                //int id = reader.GetInt32(0);
                                int id = reader.GetInt32(reader.GetOrdinal("Id"));
                                string name = reader.GetString(reader.GetOrdinal("Name"));
                                int age = reader.GetInt32(reader.GetOrdinal("Age"));
                                Console.WriteLine("{0}: {1} is {2} y/o", id, name, age);
                            }
                            else
                            {
                                Console.WriteLine("Record not found");
                            }
                        }
                    }
                }
/************************************************************************************************************/
            }
            finally
            {
                Console.WriteLine("Press any key");
                Console.ReadLine();
            }
        }
    }
}
